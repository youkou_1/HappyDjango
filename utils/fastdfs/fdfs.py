#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
  @Time : 2019/1/12 22:22 
  @Auth : Youkou 
  @Site : www.youkou.site
  @File : fdfs.py
  @IDE  : PyCharm
  @Edit : 2019/1/12
-------------------------------------------------
"""
from fdfs_client.client import Fdfs_client


# 指定fdfs客户端配置文件所在路径
FDFS_Client = Fdfs_client('utils/fastdfs/client.conf')
