#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
  @Time : 2019/1/18 22:17 
  @Auth : Youkou 
  @Site : www.youkou.site
  @File : test.py.py
  @IDE  : PyCharm
  @Edit : 2019/1/18
-------------------------------------------------
"""


def application(env, start_response):
    start_response('200 OK', [('Content-Type', 'text/html')])
    return [b"Hello World"]
