import requests

from django.shortcuts import render
from django.views import View
from django.http import Http404, FileResponse
from django.utils.encoding import escape_uri_path
from django.conf import settings

from .models import Doc


def doc_index(request):
    """
    create doc index veiw
    :param request:
    :return:
    """
    docs = Doc.objects.defer("author", "create_time", "update_time", "is_delete").filter(is_delete=False)
    return render(request, 'doc/docDownload.html', locals())


class DocDownload(View):
    """
    create doc download
    route: /docs/<int:doc_id>/
    """
    def get(self, request, doc_id):
        doc = Doc.objects.only('file_url', 'title').filter(is_delete=False, id=doc_id).first()
        if doc:
            doc_url = doc.file_url
            doc_url = settings.SITE_DOMAIN_PORT + doc_url
            doc_name = doc.title
            # /home/Conner/HappyDjango/media/django项目班_英语单词2.doc
            # media/django项目班_英语单词2.doc
            res = FileResponse(open(doc.file_url[1:], 'rb'))
            # res = FileResponse(requests.get(doc_url, stream=True))

            ex_name = doc_url.split('.')[-1]
            if not ex_name:
                raise Http404("<h1>文档url异常</h1>")
            else:
                ex_name = ex_name.lower()

            if ex_name == "pdf":
                res["Content-type"] = "application/pdf"
            elif ex_name == "zip":
                res["Content-type"] = "application/zip"
            elif ex_name == "doc":
                res["Content-type"] = "application/msword"
            elif ex_name == "xls":
                res["Content-type"] = "application/vnd.ms-excel"
            elif ex_name == "docx":
                res["Content-type"] = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
            elif ex_name == "ppt":
                res["Content-type"] = "application/vnd.ms-powerpoint"
            elif ex_name == "pptx":
                res["Content-type"] = "application/vnd.openxmlformats-officedocument.presentationml.presentation"
            else:
                raise Http404("<h1>文档格式不正确</h1>")

            final_filename = doc_name + '.' + ex_name
            # doc_filename = escape_uri_path(doc_url.split('/')[-1])
            doc_filename = escape_uri_path(final_filename)
            # 设置为inline，会直接打开
            res["Content-Disposition"] = "attachment; filename*=UTF-8''{}".format(doc_filename)
            return res

        else:
            raise Http404("<h1>文档不存在！</h1>")
