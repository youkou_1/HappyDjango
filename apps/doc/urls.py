#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
  @Time : 2019/2/22 21:23 
  @Auth : Youkou 
  @Site : www.youkou.site
  @File : urls.py
  @IDE  : PyCharm
  @Edit : 2019/2/22
-------------------------------------------------
"""
from django.urls import path

from . import views

app_name = 'doc'

urlpatterns = [
    path('', views.doc_index, name='index'),    # 主页路由
    path('<int:doc_id>/', views.DocDownload.as_view(), name='doc_download'),

]
