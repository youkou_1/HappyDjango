from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager as _UserManager


class UserManager(_UserManager):

    def create_superuser(self, username, password, email=None, **extra_fields):
        super(UserManager, self).create_superuser(username=username, password=password, email=email, **extra_fields)


class Users(AbstractUser):
    mobile = models.CharField(max_length=11, unique=True, verbose_name="手机号", help_text="手机号",
                              error_messages={"unique": "此手机号已注册"})
    email_active = models.BooleanField(default=False, verbose_name="邮箱验证状态")

    REQUIRED_FIELDS = ['mobile']
    objects = UserManager()

    class Meta:
        # users_users
        # tb_users
        db_table = "tb_users"
        verbose_name = "用户"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.username
