#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
  @Time : 2019/2/22 21:23 
  @Auth : Youkou 
  @Site : www.youkou.site
  @File : urls.py
  @IDE  : PyCharm
  @Edit : 2019/2/22
-------------------------------------------------
"""
from django.urls import path

from . import views

app_name = 'users'

urlpatterns = [
    path('register/', views.RegisterView.as_view(), name='register'),    # 主页路由
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),

]
