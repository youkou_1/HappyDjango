#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
  @Time : 2019/2/26 22:27 
  @Auth : Youkou 
  @Site : www.youkou.site
  @File : constantS.PY.py
  @IDE  : PyCharm
  @Edit : 2019/2/26
-------------------------------------------------
"""
# 图片验证码redis有效期，单位秒
IMAGE_CODE_REDIS_EXPIRES = 5 * 60

SMS_CODE_NUMS = 6

# 短信验证码有效期，单位为秒
SMS_CODE_REDIS_EXPIRES = 5 * 60

# 发送间隔
SEND_SMS_CODE_INTERVAL = 60

# 云通讯短信验证码过期时间（发送短信是显示为5分钟）
SMS_CODE_YUNTX_EXPIRES = 5

# 短信发送模板
SMS_CODE_TEMP_ID = 1
