#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
  @Time : 2019/2/22 21:23 
  @Auth : Youkou 
  @Site : www.youkou.site
  @File : urls.py
  @IDE  : PyCharm
  @Edit : 2019/2/22
-------------------------------------------------
"""
from django.urls import path, re_path

from . import views

app_name = 'verifications'

urlpatterns = [
    path('image_codes/<uuid:image_code_id>/', views.ImageCode.as_view(), name='image_code'),    # 主页路由
    re_path(r'usernames/(?P<username>\w{5,20})/', views.CheckUsernameView.as_view(), name='check_username'),
    re_path('mobiles/(?P<mobile>1[3-9]\d{9})/', views.CheckMobileView.as_view(), name='check_mobiles'),
    path('sms_codes/', views.SmsCodesView.as_view(), name='sms_codes'),

]
