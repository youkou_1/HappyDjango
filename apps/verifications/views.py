import logging
import json
import random
import string

from django.shortcuts import render
from django.views import View
from django_redis import get_redis_connection
from django.http import HttpResponse, JsonResponse

from utils.captcha.captcha import captcha
from . import constants
from users.models import Users
from utils.res_code import Code, error_map
from utils.json_fun import to_json_data
from . import forms
from utils.yuntongxun.sms import CCP

logger = logging.getLogger('django')


class ImageCode(View):
    """create image code
     # 1、创建一个类视图
    """

    def get(self, request, image_code_id):
        # 2、从前端获取参数
        # 3、校验参数
        # 4、生成图片验证码图片以及图片验证码文本
        text, image = captcha.generate_captcha()
        logger.info("Image code: {}".format(text))
        # 5、保存图片验证码文本到redis
        con_redis = get_redis_connection(alias='verify_code')
        img_key = "img_{}".format(image_code_id).encode('utf-8')
        con_redis.setex(img_key, constants.IMAGE_CODE_REDIS_EXPIRES, text)

        # 6、返回图片验证码图片给前端
        return HttpResponse(content=image, content_type="image/jpg")


class CheckUsernameView(View):
    """
    check username whether existed
    route: /usernames/(?P<username>\w{5,20})/
    # 1,创建一个类视图
    """
    def get(self, request, username):
        # 2.从前端获取参数
        # 3.校验参数
        # 4.判断用户名是否已注册
        # k = 1
        data = {
            'username': username,
            'count': Users.objects.filter(username=username).count()
        }
        # try:
        #     user = Users.objects.get(username=username)
        # except Exception as e:
        #     return to_json_data(errno=Code)
        # 5.将json格式的数据返回给前端
        return to_json_data(data=data)


class CheckMobileView(View):
    """
    验证注册的手机号是否存在
    GET mobiles/(?P<mobile>1[3-9]\d{9})/
    """

    def get(self, request, mobile):

        data = {
            'mobile': mobile,
            'count': Users.objects.filter(mobile=mobile).count()
        }
        return to_json_data(data=data)


class SmsCodesView(View):
    """
    send mobile sms code
    POST /sms_codes/
    # 1.创建类视图
    """
    def post(self, request):
        # 2.获取前端传过来的json格式数据
        json_data = request.body
        if not json_data:
            return to_json_data(errno=Code.PARAMERR, errmsg=error_map[Code.UNKOWNERR])
        dict_data = json.loads(json_data.decode('utf8'))

        # 3.校验参数
        # mobile = dict_data.get('mobile')
        # image_code_id = dict_data.get('image_code_id')
        # text = dict_data.get('text')
        # if 手机号是否为空:
        #     if 手机号长度是否为11位:
        #         pass
        #     else:
        #         return 手机号格式不正确
        # else:
        #     return 手机号没有输入
        # form, Django restframework serialize序列化器
        form = forms.CheckImgCodeForm(data=dict_data)
        if form.is_valid():
            # 获取手机号
            mobile = form.cleaned_data.get('mobile')
            # 4.发送短信验证码
            # 5.将短信验证码文本和发送短信验证码记录保存到redis数据库
            sms_num = ''.join(random.choice(string.digits) for _ in range(constants.SMS_CODE_NUMS))
            redis_conn = get_redis_connection(alias='verify_code')
            pl = redis_conn.pipeline()
            sms_flag_fmt = "sms_flag_{}".format(mobile)
            sms_text_fmt = "sms_{}".format(mobile)
            # redis_conn.setex(sms_flag_fmt, constants.SEND_SMS_CODE_INTERVAL, 1)
            # redis_conn.setex(sms_text_fmt, constants.SMS_CODE_REDIS_EXPIRES, sms_num)

            try:
                pl.setex(sms_flag_fmt, constants.SEND_SMS_CODE_INTERVAL, 1)
                pl.setex(sms_text_fmt, constants.SMS_CODE_REDIS_EXPIRES, sms_num)
                pl.execute()
            except Exception as e:
                logger.debug("redis 执行出现异常：{}".format(e))
                return to_json_data(errno=Code.UNKOWNERR, errmsg=error_map[Code.UNKOWNERR])

            logger.info("发送验证码短信[正常][ mobile: %s sms_code: %s]" % (mobile, sms_num))
            return to_json_data(errmsg="短信验证码发送成功")
            # try:
            #     result = CCP().send_template_sms(mobile, [sms_num, constants.SMS_CODE_YUNTX_EXPIRES],
            #                                      constants.SMS_CODE_TEMP_ID)
            # except Exception as e:
            #     logger.error("发送验证码短信[异常][ mobile: %s, message: %s ]" % (mobile, e))
            #     return to_json_data(errno=Code.SMSERROR, errmsg=error_map[Code.SMSERROR])
            # else:
            #     if result == 0:
            #         logger.info("发送验证码短信[正常][ mobile: %s sms_code: %s]" % (mobile, sms_num))
            #         return to_json_data(errmsg="短信验证码发送成功")
            #     else:
            #         logger.warning("发送验证码短信[失败][ mobile: %s ]" % mobile)
            #         return to_json_data(errno=Code.SMSFAIL, errmsg=error_map[Code.SMSFAIL])

        else:
            # 定义一个错误信息列表
            err_msg_list = []
            for item in form.errors.get_json_data().values():
                err_msg_list.append(item[0].get('message'))
                # print(item[0].get('message'))   # for test
            err_msg_str = '/'.join(err_msg_list)  # 拼接错误信息为一个字符串
            # 6.将json格式数据返回给前端
            return to_json_data(errno=Code.PARAMERR, errmsg=err_msg_str)
