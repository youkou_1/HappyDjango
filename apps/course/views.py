import logging

from django.shortcuts import render
from django.http import Http404
from django.views import View

logger = logging.getLogger('django')


from . import models


def course_list(request):
    """

    :param request:
    :return:
    """
    courses = models.Course.objects.only('title', 'cover_url', 'teacher__positional_title').filter(is_delete=False)
    return render(request, 'course/course.html', locals())


class CourseDetailView(View):
    """
    """
    def get(self, request, course_id):
        try:
            course = models.Course.objects.only('title', 'cover_url', 'video_url', 'profile', 'outline',
                                                'teacher__name', 'teacher__avatar_url',
                                                'teacher__positional_title', 'teacher__profile').\
                select_related('teacher').get(is_delete=False, id=course_id)
            return render(request, 'course/course_detail.html', locals())
        except models.Course.DoesNotExist as e:
            logger.info("当前课程出现如下异常：\n{}".format(e))
            raise Http404("此课程不存在！")
