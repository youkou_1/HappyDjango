#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
  @Time : 2019/2/22 21:23 
  @Auth : Youkou 
  @Site : www.youkou.site
  @File : urls.py
  @IDE  : PyCharm
  @Edit : 2019/2/22
-------------------------------------------------
"""
from django.urls import path

from . import views

app_name = 'news'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),    # 主页路由
    path('news/', views.NewsListView.as_view(), name='news_list'),
    path('news/banners/', views.NewsBanner.as_view(), name='news_banner'),
    path('news/<int:news_id>/', views.NewsDetailView.as_view(), name='news_detail'),
    path('news/<int:news_id>/comments/', views.NewsCommentView.as_view(), name='news_comment'),
    path('search/', views.SearchView(), name='search'),

]
